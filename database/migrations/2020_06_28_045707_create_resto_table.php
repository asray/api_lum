<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;

class CreateRestoTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('resto', function (Blueprint $table) {
            $table->bigIncrements('id');
            $table->string('username');
            $table->string('pass');
            $table->string('email');
            $table->string('phone');
            $table->string('name');
            $table->text('address');
            $table->string('logo');
            $table->string('cp');
            $table->text('branch');


            $table->timestamps();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('resto');
    }
}
