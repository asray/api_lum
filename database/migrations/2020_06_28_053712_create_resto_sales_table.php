<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;

class CreateRestoSalesTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('resto_sales', function (Blueprint $table) {
            $table->bigIncrements('id');
            $table->integer('resto_admin_id');
            $table->integer('customer_id');
            $table->date('date');
            $table->string('number');
            $table->integer('status');
            $table->string('voucher');
            $table->string('discount');
            $table->string('description');
            $table->integer('payment_method');
            $table->integer('deliver_method');
            $table->timestamps();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('resto_sales');
    }
}
