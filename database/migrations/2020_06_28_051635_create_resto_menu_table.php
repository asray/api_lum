<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;

class CreateRestoMenuTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('resto_menu', function (Blueprint $table) {
            $table->bigIncrements('id');
            $table->integer('id_resto');
            $table->string('menu');
            $table->integer('price');
            $table->integer('is_able');
            $table->integer('trait');
            $table->text('pic');
            $table->integer('rating');

            $table->timestamps();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('resto_menu');
    }
}
