<?php

use Illuminate\Database\Seeder;

class Resto_adminTableSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        DB::table('resto_admin')->truncate();

        DB::table('resto_admin')->insert(
            [
                [
                'id'=> '1',
                'username'=> 'a',
                'email'=> 'a@gmail.com',
                'phone'=> '1',
                'name'=> 'a',
                'created_at'=> '2020-06-22 20:47:54',
                'updated_at'=> '2020-06-26 06:21:49'
                ],                             
            ]
        );
    }
}