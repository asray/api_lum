<?php

use Illuminate\Database\Seeder;

class SalesTableSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        DB::table('sales')->truncate();

        DB::table('sales')->insert(
            [
                [
                'id'=> '1',
                'admin_id'=> '1',
                'date'=> '2020-06-28',
                'number'=> '1',
                'voucher'=> '1',
                'discount'=> '1',
                'description'=> 'a',
                'created_at'=> '2020-06-22 20:47:54',
                'updated_at'=> '2020-06-26 06:21:49'
                ],                             
            ]
        );
    }
}