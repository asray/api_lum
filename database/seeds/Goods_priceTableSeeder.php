<?php

use Illuminate\Database\Seeder;

class Goods_priceTableSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        DB::table('goods_price')->truncate();

        DB::table('goods_price')->insert(
            [
                [
                'id'=> '1',
                'goods_id'=> '1',
                'price'=> '1',
                'discount'=> '1',               
                'created_at'=> '2020-06-22 20:47:54',
                'updated_at'=> '2020-06-26 06:21:49'
                ],                             
            ]
        );
    }
}
