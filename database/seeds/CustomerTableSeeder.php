<?php

use Illuminate\Database\Seeder;

class CustomerTableSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        DB::table('customer')->truncate();

        DB::table('customer')->insert(
            [
                [
                'id'=> '1',
                'username'=> 'a',
                'pass'=> 'a',
                'email'=> 'a@gmail.com',
                'phone'=> '1',
                'name'=> 'a',
                'address'=> 'a',

                /**'created_at'=> '2020-06-22 20:47:54',
                'updated_at'=> '2020-06-26 06:21:49'*/
                ],                             
            ]
        );
    }
}