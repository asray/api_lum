<?php

use Illuminate\Database\Seeder;

class Sales_detailTableSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        DB::table('sales_detail')->truncate();

        DB::table('sales_detail')->insert(
            [
                [
                'id'=> '1',
                'sales_id'=> '1',
                'goods_id'=> '1',
                'qty'=> '1',
                'price'=> '1',
                'created_at'=> '2020-06-22 20:47:54',
                'updated_at'=> '2020-06-26 06:21:49'
                ],                             
            ]
        );
    }
}