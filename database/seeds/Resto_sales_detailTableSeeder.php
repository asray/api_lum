<?php

use Illuminate\Database\Seeder;

class Resto_sales_detailTableSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        DB::table('resto_sales_detail')->truncate();

        DB::table('resto_sales_detail')->insert(
            [
                [
                'id'=> '1',
                'resto_sales_id'=> '1',
                'resto_menu_id'=> '1',
                'qty'=> '1',
                'price'=> '1',
                'created_at'=> '2020-06-22 20:47:54',
                'updated_at'=> '2020-06-26 06:21:49'
                ],                             
            ]
        );
    }
}

