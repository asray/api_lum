<?php

use Illuminate\Database\Seeder;
use Illuminate\Support\Facades\DB;

class AdminTableSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        DB::table('admin')->truncate();

        DB::table('admin')->insert(
            [
                [
                'id'=> '1',
                'username'=> 'a',
                'email'=> 'a',
                'phone'=> 'a',
                'name'=> 'a',
                'created_at'=> '2020-06-22 20:47:54',
                'updated_at'=> '2020-06-26 06:21:49'
                ],                             
            ]
        );

    }
}
