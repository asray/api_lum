<?php

use Illuminate\Database\Seeder;

class Customer_feedbackTableSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        DB::table('customer_feedback')->truncate();

        DB::table('customer_feedback')->insert(
            [
                [
                'id'=> '1',
                'customer_id'=> 'a',
                'resto_menu_id'=> 'a',
                'rating'=> 'a',
                'description'=> 'a',
                /**'created_at'=> '2020-06-22 20:47:54',
                'updated_at'=> '2020-06-26 06:21:49'*/
                ],                             
            ]
        );
    }
}
