<?php

use Illuminate\Database\Seeder;

class Resto_salesTableSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        DB::table('resto_sales')->truncate();

        DB::table('resto_sales')->insert(
            [
                [
                'id'=> '1',
                'resto_admin_id'=> '1',
                'customer_id'=> '1',
                'date'=> '2020-06-28',
                'number'=> '1',
                'status'=> '1',
                'voucher'=> '1',
                'discount'=> '1',
                'description'=> 'a',
                'payment_method'=> '1',
                'deliver_method'=> '1',
                'created_at'=> '2020-06-22 20:47:54',
                'updated_at'=> '2020-06-26 06:21:49'
                ],                             
            ]
        );
    }
}
