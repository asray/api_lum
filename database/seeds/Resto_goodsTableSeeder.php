<?php

use Illuminate\Database\Seeder;

class Resto_goodsTableSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        DB::table('resto_goods')->truncate();

        DB::table('resto_goods')->insert(
            [
                [
                'id'=> '1',
                'resto_id'=> '1',
                'supplier_goods_id'=> '1',
                'stock'=> '1',
                'created_at'=> '2020-06-22 20:47:54',
                'updated_at'=> '2020-06-26 06:21:49'
                ],                             
            ]
        );
    }
}