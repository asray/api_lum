<?php

use Illuminate\Database\Seeder;

class Resto_menuTableSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        DB::table('resto_menu')->truncate();

        DB::table('resto_menu')->insert(
            [
                [
                'id'=> '1',
                'id_resto'=> '1',
                'menu'=> '1',
                'price'=> '1',
                'is_able'=> '1',
                'trait'=> '1',
                'pic'=> '1',
                'rating'=> '1',

                'created_at'=> '2020-06-22 20:47:54',
                'updated_at'=> '2020-06-26 06:21:49'
                ],                             
            ]
        );
    }
}
