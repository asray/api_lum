<?php

use Illuminate\Database\Seeder;

class Supplier_goodsTableSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        DB::table('supplier_goods')->truncate();

        DB::table('supplier_goods')->insert(
            [
                [
                'id'=> '1',
                'supplier_id'=> '1',
                'name'=> '1',
                'stock'=> '1',
                'price'=> '1',
                'created_at'=> '2020-06-22 20:47:54',
                'updated_at'=> '2020-06-26 06:21:49'
                ],                             
            ]
        );
    }
}