<?php

/*
|--------------------------------------------------------------------------
| Application Routes
|--------------------------------------------------------------------------
|
| Here is where you can register all of the routes for an application.
| It is a breeze. Simply tell Lumen the URIs it should respond to
| and give it the Closure to call when that URI is requested.
|
*/

$router->get('/', function () use ($router) {
    return $router->app->version();
});

$router->get('user', 'UserController@index');
$router->get('user/tampil', 'UserController@getIndexViews');

$router->get("admin", "AdminController@index");
$router->post("admin/insert", "AdminController@store");
$router->patch("admin/update", "AdminController@update");
$router->delete("admin/{id}", "AdminController@destroy");

$router->get("customer", "CustomerController@index");
$router->post("customer/insert", "CustomerController@store");
$router->patch("customer/update", "CustomerController@update");
$router->delete("customer/{id}", "CustomerController@destroy");

$router->get("customer_feedback", "Customer_feedbackController@index");
$router->post("customer_feedback/insert", "CustomerController@store");
$router->patch("customer_feedback/update", "CustomerController@update");
$router->delete("customer_feedback/{id}", "CustomerController@destroy");

