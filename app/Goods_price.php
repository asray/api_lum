<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class Goods_price extends Model

{

    protected $table = "goods_price";

    protected $fillable = [
        'name', 'goods_id', 'price', 'discount'
    ];
}
