<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class Resto extends Model

{

    protected $table = "resto";

    protected $fillable = [
        'username', 'pass', 'email', 'phone', 'name', 'address', 'logo', 'cp', 'branch'
    ]; 
}
