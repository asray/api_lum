<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class Supplier extends Model

{

    protected $table = "supplier";

    protected $fillable = [
        'username', 'pass', 'email', 'phone', 'name', 'logo', 'address', 'cp'
    ]; 
}
