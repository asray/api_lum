<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class Resto_sales extends Model

{

    protected $table = "resto_sales";

    protected $fillable = [
        'resto_admin_id', 'customer_id', 'date', 'number', 'status', 'voucher', 'discount', 'description', 'payment_method', 'deliver_method'
    ]; 
}
