<?php

namespace App\Http\Controllers;

class UserController extends Controller
{
    /**
     * Create a new controller instance.
     *
     * @return void
     */
    public function __construct()
    {
        return "Testing";
    }

    public function index()
    {
        return "Test";
    }

    public function getIndexViews()
    {
        return view('user');
    }
}
