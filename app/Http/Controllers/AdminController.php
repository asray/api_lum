<?php

namespace App\Http\Controllers;

use App\Admin;
use Illuminate\Http\Request;
use Illuminate\Support\Str;
use Illuminate\Validation\Validator;


class AdminController extends Controller
{
    public function index()
    {
        $getAdmin = Admin::OrderBy("id", "DESC")->paginate(10);

        $out = [
            "message" => "list_admin",
            "resutls" => $getAdmin
        ];

        return response()->json($out, 200);
    }

    public function store(Request $request)
    {

        if ($request->isMethod('post')) {

            $this->validate($request, [
                'username' => 'required',
                'email' => 'required',
                'phone' => 'required',
                'name' => 'required'

            ]);

            $username = $request->input('username');
            $email = $request->input('email');
            $phone = $request->input('phone');
            $name = $request->input('name');

            $data = [
                'username' => $username,
                'email' => $email,
                'phone' => $phone,
                'name' => $name

            ];

            $insert = Admin::create($data);

            if ($insert) {
                $out  = [
                    "message" => "success_insert_data",
                    "results" => $data,
                    "code"  => 200
                ];
            } else {
                $out  = [
                    "message" => "vailed_insert_data",
                    "results" => $data,
                    "code"   => 404,
                ];
            }

            return response()->json($out, $out['code']);
        }
    }
    public function update(Request $request)
    {

        if ($request->isMethod('patch')) {

            $this->validate($request, [
                'username' => 'required',
                'email' => 'required',
                'phone' => 'required',
                'name' => 'required',
                'id'    => 'required'
            ]);
            $id = $request->input('id');
            $username = $request->input('username');
            $email = $request->input('email');
            $phone = $request->input('phone');
            $name = $request->input('name');

            $post = Post::find($id);

            $data = [
                'username' => $username,
                'email' => $email,
                'phone' => $phone,
                'name' => $name
            ];

            $update = $post->update($data);

            if ($update) {
                $out  = [
                    "message" => "success_update_data",
                    "results" => $data,
                    "code"  => 200
                ];
            } else {
                $out  = [
                    "message" => "vailed_update_data",
                    "results" => $data,
                    "code"   => 404,
                ];
            }

            return response()->json($out, $out['code']);
        }
    }

    public function destroy($id)
    {
        $admin =  Admin::find($id);

        if (!$admin) {
            $data = [
                "message" => "id nost found",
            ];
        } else {
            $admin->delete();
            $data = [
                "message" => "success_deleted"
            ];
        }

        return response()->json($data, 200);
    }
}
