<?php

namespace App\Http\Controllers;

use App\Customer_feedback;
use Illuminate\Http\Request;
use Illuminate\Support\Str;
use Illuminate\Validation\Validator;


class Customer_feedbackController extends Controller
{
    public function index()
    {
        $getCustomer_feedback = Customer_feedback::OrderBy("id", "DESC")->paginate(10);

        $out = [
            "message" => "list Customer feedback",
            "resutls" => $getCustomer_feedback
        ];

        return response()->json($out, 200);
    }

    public function store(Request $request)
    {

        if ($request->isMethod('post')) {

            $this->validate($request, [
                'customer_id' => 'required',
                'resto_menu_id' => 'required',
                'rating' => 'required',
                'description' => 'required'

            ]);

            $customer_id = $request->input('customer_id');
            $resto_menu_id = $request->input('resto_menu_id');
            $rating = $request->input('rating');
            $description = $request->input('description');

            $data = [
                'customer_id' => $customer_id,
                'resto_menu_id' => $resto_menu_id,
                'rating' => $rating,
                'description' => $description

            ];

            $insert = Admin::create($data);

            if ($insert) {
                $out  = [
                    "message" => "success_insert_data",
                    "results" => $data,
                    "code"  => 200
                ];
            } else {
                $out  = [
                    "message" => "vailed_insert_data",
                    "results" => $data,
                    "code"   => 404,
                ];
            }

            return response()->json($out, $out['code']);
        }
    }
    public function update(Request $request)
    {

        if ($request->isMethod('patch')) {

            $this->validate($request, [
                'customer_id' => 'required',
                'resto_menu_id' => 'required',
                'rating' => 'required',
                'description' => 'required'

            ]);
            $id = $request->input('id');
            $customer_id = $request->input('customer_id');
            $resto_menu_id = $request->input('resto_menu_id');
            $rating = $request->input('rating');
            $description = $request->input('description');

            $post = Post::find($id);

            $data = [
                'customer_id' => $customer_id,
                'resto_menu_id' => $resto_menu_id,
                'rating' => $rating,
                'description' => $description

            ];
            $update = $post->update($data);

            if ($update) {
                $out  = [
                    "message" => "success_update_data",
                    "results" => $data,
                    "code"  => 200
                ];
            } else {
                $out  = [
                    "message" => "vailed_update_data",
                    "results" => $data,
                    "code"   => 404,
                ];
            }

            return response()->json($out, $out['code']);
        }
    }

    public function destroy($id)
    {
        $customer_feedback =  Customer_feedback::find($id);

        if (!$customer_feedback) {
            $data = [
                "message" => "id nost found",
            ];
        } else {
            $customer_feedback->delete();
            $data = [
                "message" => "success_deleted"
            ];
        }

        return response()->json($data, 200);
    }
}
