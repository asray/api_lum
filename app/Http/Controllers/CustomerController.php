<?php

namespace App\Http\Controllers;

use App\Customer;
use Illuminate\Http\Request;
use Illuminate\Support\Str;
use Illuminate\Validation\Validator;


class CustomerController extends Controller
{
    public function index()
    {
        $getCustomer = Customer::OrderBy("id", "DESC")->paginate(10);

        $out = [
            "message" => "list_customer",
            "resutls" => $getCustomer
        ];

        return response()->json($out, 200);
    }

    public function store(Request $request)
    {

        if ($request->isMethod('post')) {

            $this->validate($request, [
                'username' => 'required',
                'email' => 'required'
            ]);

            $username = $request->input('username');
            $email = $request->input('email');

            $data = [
                'username'  => Str::slug($username, "-"),
                'email' => $email,
                'phone' => $phone,
                'name' => $name

            ];

            $insert = Admin::create($data);

            if ($insert) {
                $out  = [
                    "message" => "success_insert_data",
                    "results" => $data,
                    "code"  => 200
                ];
            } else {
                $out  = [
                    "message" => "vailed_insert_data",
                    "results" => $data,
                    "code"   => 404,
                ];
            }

            return response()->json($out, $out['code']);
        }
    }
    public function update(Request $request)
    {

        if ($request->isMethod('patch')) {

            $this->validate($request, [
                'title' => 'required',
                'body'  => 'required',
                'id'    => 'required'
            ]);
            $id = $request->input('id');
            $title = $request->input('title');
            $body = $request->input('body');

            $post = Post::find($id);

            $data = [
                'slug'  => Str::slug($title, "-"),
                'title' => $title,
                'body' => $body
            ];

            $update = $post->update($data);

            if ($update) {
                $out  = [
                    "message" => "success_update_data",
                    "results" => $data,
                    "code"  => 200
                ];
            } else {
                $out  = [
                    "message" => "vailed_update_data",
                    "results" => $data,
                    "code"   => 404,
                ];
            }

            return response()->json($out, $out['code']);
        }
    }

    public function destroy($id)
    {
        $posts =  Post::find($id);

        if (!$posts) {
            $data = [
                "message" => "id nost found",
            ];
        } else {
            $posts->delete();
            $data = [
                "message" => "success_deleted"
            ];
        }

        return response()->json($data, 200);
    }
}
