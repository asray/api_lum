<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class Resto_sales_detail extends Model

{

    protected $table = "resto_sales_detail";

    protected $fillable = [
        'resto_sales_id', 'resto_menu_id', 'qty', 'price'
    ]; 
}
