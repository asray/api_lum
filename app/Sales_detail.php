<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class Sales_detail extends Model

{

    protected $table = "sales_detail";

    protected $fillable = [
        'sales_id', 'goods_id', 'qty', 'price'
    ]; 
}
