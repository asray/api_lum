<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class Resto_admin extends Model

{

    protected $table = "resto_admin";

    protected $fillable = [
        'name', 'username', 'email', 'phone', 'name'
    ]; 
}
