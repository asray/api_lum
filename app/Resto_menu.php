<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class Resto_menu extends Model

{

    protected $table = "resto_goods";

    protected $fillable = [
        'id_resto', 'menu', 'price', 'is_able', 'trait', 'pic', 'rating'
    ]; 
}
