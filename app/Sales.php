<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class Sales extends Model

{

    protected $table = "sales";

    protected $fillable = [
        'admin_id', 'date', 'number', 'voucher', 'discount', 'description'
    ]; 
}
