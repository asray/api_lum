<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class Resto_goods extends Model

{

    protected $table = "resto_goods";

    protected $fillable = [
        'resto_id', 'supplier_goods_id', 'stock'
    ]; 
}
