<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class Supplier_goods extends Model

{

    protected $table = "supplier_goods";

    protected $fillable = [
        'supplier_id', 'name', 'stock', 'price'
    ]; 
}
