<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class Customer_feedback extends Model

{

    protected $table = "customer_feedback";

    protected $fillable = [
        'customer_id', 'resto_menu_id', 'rating', 'description'
    ];
}
